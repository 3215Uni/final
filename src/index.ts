//console.log('hello typescript 3');
import "reflect-metadata";
import app from './app';
import { AppDataSource } from "./db";
//Aqui se conecta al puerto de endPOINT
const port = 3000
//funcion principal 
async function main() {
  try {
    //inicializacion  del dataSource
    await AppDataSource.initialize();
    console.log('Database Connected...');
    //con esta linea arrancamo la consola con el puerto en q s eencuentra
    app.listen(port, () => console.log(`App listening on port ${port}!`))
  } catch (error) {
    console.error(error);
  }
}

main();