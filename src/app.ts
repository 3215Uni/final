import express from 'express'
import morgan from 'morgan';
import cors from 'cors';
//Aqui es la ubicacion desde donde se importo  la diereccion esta en directorio routes, el archivo user.router.ts,event.router.ts,booking.router.ts
import userRoutes from './routes/user.router';
//librerias
import passportMiddleware from './middlewares/passport';
import passport from 'passport';
import passportLocal from 'passport-local';

import eventRoutes from "./routes/event.router";
import bookingRoutes from "./routes/booking.router";

const app = express()

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

/*agreger JWT para la contraseña usa insign
 //El urlencode se usa similar al express,
 si mansdan un dato en el header Utorizechion pueden acceder al token
*/
app.use(express.urlencoded({extended:false}));
app.use(passport.initialize());
passport.use(passportMiddleware);

//aqui se le agrega la ruta de User Event, Booking
app.use("/api", userRoutes);
app.use("/api", eventRoutes);
app.use("/api", bookingRoutes);

export default app;

/*import express from 'express'
import morgan from 'morgan';
import cors from 'cors';
import userRoutes from "./routes/user.router";
const app = express()

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

app.use("/api", userRoutes);

export default app;
*/