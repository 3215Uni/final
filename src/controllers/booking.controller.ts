import { Request, Response } from "express";
import { Booking } from "../entity/Booking";

interface BookingBody {
  id_event: number;
  id_user: number;
  precio: number;
  fechaHora: Date;
  lugar: string;
  gps: string;
}
//aqui consulto todos los Booking 
export const getBookings = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
      //find crea un select automaticamente 
      const bookings = await Booking.find();
      console.log('bookings: --->'), bookings;
      //nos retorna un users
      return res.json(bookings);
      //y si es un erro me retonrna un 500
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    } };



//aqui consulto los Booking con ID en particular
export const getBooking = async (req: Request, res: Response) => {
    try {//sacamos un id en particular
      const { id } = req.params;
      //si quiero buscar por nombre pongo nombre en vez de id
      const booking = await Booking.findOneBy({ id: parseInt(id) });
  
      if (!booking) return res.status(404).json({ message: "User not found" });
  
      return res.json(booking);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };
export const createBooking = async (req: Request, res: Response) => {
  try {
    const { id_event, id_user, precio, fechaHora, lugar, gps } = req.body as BookingBody;

    const booking = Booking.create({
      id_event,
      id_user,
      precio,
      fechaHora,
      lugar,
      gps,
    });

    await booking.save();

    return res.status(201).json(booking);
  } catch (error) {
    return res.status(500).json({ error: "Error al crear la reserva" });
  }
};

//updateBooking
export const updateBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
  //en el try primero debo buscar el objeto para poder modificarlo por eso lo busco con find
    try {
      //es para modificar el booking con ese id
      const user = await Booking.findOneBy({ id: parseInt(id) });
      //si no existe el booking me retorna un 404
      if (!user) return res.status(404).json({ message: "Not Booking found" });
  //si existe el booking,le pasamos los parametros y el req body update es uan funcion que me devuelve
      await Booking.update({ id: parseInt(id) }, req.body);
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };
//deleteBooking
export const deleteBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    //dentro del try 
    try {
      //con esa line delte borra el campo clave de la tabla
      const result = await Booking.delete({ id: parseInt(id) });
  
      if (result.affected === 0)
        return res.status(404).json({ message: "User not found" });
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };