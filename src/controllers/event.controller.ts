import { Request, Response } from "express";
import { Event, EventType } from "../entity/Event";

interface EventBody {
  nombre: string;
  descripcion: string;
  lugar: string;
  fechaHora: Date;
  gps: string;
  precio: number;
  limite: number;
  tipoevento: EventType;
}

//aqui consulto todos los users 
export const getEvents = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
      //find crea un select automaticamente 
      const events = await Event.find();
      console.log('events: --->'), events;
      //nos retorna un users
      return res.json(events);
      //y si es un erro me retonrna un 500
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    } };
    

//aqui consulto los event con ID en particular
export const getEvent = async (req: Request, res: Response) => {
    try {//sacamos un id en particular
      const { id } = req.params;
      //si quiero buscar por nombre pongo nombre en vez de id
      const event = await Event.findOneBy({ id: parseInt(id) });
  
      if (!event) return res.status(404).json({ message: "User not found" });
  
      return res.json(event);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };
  
  
//CreateEvent
export const createEvent = async (req: Request, res: Response) => {
    const { nombre,descripcion,lugar,fechaHora,gps,precio,limite,tipoevento,} = req.body;
  //OBTENEMOS LOD VALORES o creamos objeto
    const event = new Event();  //creamos un objetos
    event.nombre = nombre;//sle asignamos email
    event.descripcion = descripcion;//sle asignamos email
    event.lugar = lugar;//sle asignamos email
    event.fechaHora = fechaHora;//sle asignamos email
    event.gps = gps;//sle asignamos email
    event.precio = precio;//le asigname password
    event.limite = limite;//
    event.tipoevento = tipoevento;//
    
  //GUARDAMOS LOS VALORES con await es una sola linea para guardar
    await event.save();
    return res.json(event);
  };  

//updateEvent
export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
  //en el try primero debo buscar el objeto para poder modificarlo por eso lo busco con find
    try {
      //es para modificar el usuario con ese id
      const user = await Event.findOneBy({ id: parseInt(id) });
      //si no existe el usuario me retorna un 404
      if (!user) return res.status(404).json({ message: "Not user found" });
  //si existe el usuario,le pasamos los parametros y el req body update es uan funcion que me devuelve
      await Event.update({ id: parseInt(id) }, req.body);
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };

//deleteEvent
export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    //dentro del try 
    try {
      //con esa line delte borra el campo clave de la tabla
      const result = await Event.delete({ id: parseInt(id) });
  
      if (result.affected === 0)
        return res.status(404).json({ message: "User not found" });
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };