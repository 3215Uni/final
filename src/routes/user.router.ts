import { Router } from "express";
import {getUsers, getUser, createUser, updateUser, deleteUser} from "../controllers/user.controller";
//import {getEvents} from "../controllers/event.controller";
import {signIn, protectedEndpoint, refresh } from '../controllers/user.controller'
import passport from 'passport'

const router = Router();

router.get("/users", getUsers);

router.get("/users/:id", getUser);
//router.post("/users", createUser);
router.put("/users/:id", updateUser);
router.delete("/users/:id", deleteUser);

//Agregar para jwt
router.post('/singup', createUser);//aqui se debe hasharlea
router.post('/signin', signIn);//comparar con el password de lOS datos, generar el token
router.post('/token', refresh);//este endpoint genera un nuveo token valido, cuando venza el token
router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);//es un endpoint q devuelve un ok, si esta correcta la llamada y la autenticación
export default router;

/*import { Router} from "express";
import {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser,
} from "../controllers/user.controller";
const router = Router();
router.get("/users", getUsers);
router.get("/users/:id", getUser);

router.post("/users", createUser);
//nos va a redirigir al id user
router.put("/users/:id", updateUser);
//nos va a redirigir ral id user
router.delete("/users/:id", deleteUser);
export default router;*/