import {Entity,Column, PrimaryGeneratedColumn,BaseEntity,CreateDateColumn,UpdateDateColumn,JoinColumn, OneToMany} from "typeorm";
//import {Entity,Column, PrimaryGeneratedColumn,BaseEntity,CreateDateColumn,UpdateDateColumn, OneToOne, JoinColumn, OneToMany} from "typeorm";
//import { Booking } from "./Booking";
  
  @Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'
  export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    username: string;
    @Column()
    password: string;
    @Column({ default: true })
    active: boolean;
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
/*
   @OneToMany(() => Booking,(booking) => booking.user)
   booking: Booking[]*/
  }
/*import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BaseEntity,
    CreateDateColumn,
    UpdateDateColumn,
  } from "typeorm";
  
  @Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'

  export class User extends BaseEntity {
    //campo id  autogenerada de clave primaria
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    firstname: string;
  
    @Column()
    lastname: string;
  
    @Column({ default: true })
    active: boolean;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  }*/