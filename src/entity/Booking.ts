//import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn ,ManyToOne, JoinColumn} from "typeorm";
//import { Event } from "./Event";
//import { User } from "./User";
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from "typeorm";

@Entity() // Nombre de la tabla
export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  id_event: number;

  @Column()
  id_user: number;

  @Column({ type: "decimal", precision: 10, scale: 2 })
  precio: number;

  @Column()
  fechaHora: Date;

  @Column()
  lugar: string;

  @Column()
  gps: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
/*
  @ManyToOne(() => Event, (event) => event.booking)
  @JoinColumn({ name: "id_event" })
  event: Event;

  @ManyToOne(() => User, (user) => user.booking)
  @JoinColumn({ name: "id_user" })
  user: User;*/
}
