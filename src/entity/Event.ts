//import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn,OneToMany } from "typeorm";
//import { Booking } from "./Booking";
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from "typeorm";
export enum EventType {
  Cine = "cine",
  Teatro = "teatro",
  Recital = "recital",
  Charla = "charla"
}

@Entity() // Puedes cambiar "eventsTable" por el nombre que desees para la tabla
export class Event extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  descripcion: string;

  @Column()
  lugar: string;

  @Column()
  fechaHora: Date;

  @Column()
  gps: string;

  @Column({ type: "decimal", precision: 10, scale: 2 })
  precio: number;

  @Column({ default: 0 })
  limite: number;

  @Column({ type: "enum", enum: EventType, default: EventType.Cine })
  tipoevento: EventType;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
/*
  @OneToMany(() => Booking, (booking) => booking.event)
  booking: Booking[]
*/
}
