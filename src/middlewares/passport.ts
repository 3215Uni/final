import { User } from "../entity/User";
import { Strategy, ExtractJwt, StrategyOptions } from "passport-jwt";


const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'somesecrettoken'
};
//funcion que cuando se inicializa en app.ts con esas opciones de configuracion pasan al payload
export default new Strategy(opts, async (payload, done) => {
  try {
    //aqui busca en la base de dato con el id 
    const user = await User.findOneBy({ id: parseInt(payload.id) });

    if (user) {
        //donde es una funcion de passport
        //si lo encuentra devuelve el usuario
      return done(null, user);
    }
    //si no encuentra el usuario devuelve un false
    return done(null, false);
    
  } catch (error) {
    console.log(error);
  }
});
