import { DataSource } from "typeorm";
import { User } from "./entity/User";
import { Event } from "./entity/Event";
import { Booking } from "./entity/Booking";
//Aqui se conecta a la base de datos
export const AppDataSource = new DataSource({
  type: "mysql",
  host: "localhost",
  //Aqui se conecta al puerto de base de datos
  port: 3306,
  username: "root",
  password: "mysql",
  database: "ticket-system-db",
   // logging: true, // muestra peticiones a la bd
   //cuando este seguro de mi base de dto recien le mando true
  synchronize: false,
  entities: [User, Event,Booking],
}); 